﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace CanteenManagementSystem.API.Interfaces
{
    public interface IGenericRepository<T> where T : class    {        T AddEntity(T entityToAdd);        Task<T> AddEntityAsync(T entityToAdd);        T GetEntityById(Guid guid);
        T GetEntityByStringId(Guid guid);        Task<T> GetEntityByIdAsync(Guid guid);        IQueryable<T> GetAll();
        IEnumerable<T> GetAllData();        Task<IEnumerable<T>> GetAllAsync();        void DeleteEntity(T entityToDelete);        Task<int> DeleteEntityAsync(T entityToDelete);        T UpdateEntity(T entityToUpdate);        IQueryable<T> FindByCondition(Expression<Func<T, bool>> predicate);        Task<IEnumerable<T>> FindByConditionAsync(Expression<Func<T, bool>> predicate);

        IQueryable<T> GetAll(
          Expression<Func<T, bool>> filter = null,
          Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null);
        void AddEntityList(IEnumerable<T> listOfEntity);
        /// <summary>        /// Use this method when inserting entity        /// but save context manually at end        /// </summary>        /// <param name="entityToAdd"></param>        /// <returns></returns>        T AddEntityWithNoSave(T entityToAdd);
        
        /// <summary>        /// Use this method when updating entity        /// but save context manually at end        /// </summary>        /// <param name="entityToUpdate"></param>        /// <returns></returns>        T UpdateEntityWithNoSave(T entityToUpdate);

        /// <summary>        /// Use this method when inserting multiple entities        /// but save context manually at end        /// </summary>        /// <param name="entityList"></param>        /// <returns></returns>        void AddEntityListWithNoSave(List<T> entityList);        void UpdateEntityListWithNoSave(List<T> entitiesList);        void DeleteEntityListWithNoSave(List<T> entitiesList);        void Save();    }
}
