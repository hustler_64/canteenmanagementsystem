﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CanteenManagementSystem.API.Data;
using CanteenManagementSystem.API.DbContext;
using CanteenManagementSystem.API.Interfaces;
using CanteenManagementSystem.API.Models;

namespace CanteenManagementSystem.API.UnitOfWorks
{
    #region Interface
    public interface IUnitOfWork
    {
        void Commit();

        IGenericRepository<AppUser> UserRepository { get; }
    }
    #endregion
    public class UnitOfWork : IUnitOfWork
    {
        private readonly CanteenManagementDbContext _context;

        public UnitOfWork(CanteenManagementDbContext context)
        {
            _context = context;
        }

        public IGenericRepository<AppUser> _AppUser;

        
        public IGenericRepository<AppUser> UserRepository
        {
            get { return _AppUser ?? (_AppUser = new GenericRepository<AppUser>(_context)); }
        }
        

        public void Commit()
        {
            _context.SaveChanges();
        }
    }
}
