﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace CanteenManagementSystem.API.DataTransferObject
{
    public class RegisterDTO
    {
        public string Username { get; set; }

        [EmailAddress]
        public string Email { get; set; }

        public string Password { get; set; }
        public string ConfirmPassword { get; set; }
    }

    public class ResetPasswordViewModel
    {
        public string Email { get; set; }
    }

    public class ForgotPasswordViewModel
    {
        public string Token { get; set; }

        public string UserId { get; set; }

        [Required(ErrorMessage = "Password required")]
        public string Password { get; set; }

        [NotMapped]
        [Required(ErrorMessage = "Confirm Password required")]
        //[Compare("Password", ErrorMessage = "Password doesn't match.")]
        public string ConfirmPassword { get; set; }

        public string Username { get; set; }
    }

    public class UpdateUser
    {
        [EmailAddress]
        public string Email { get; set; }

        public string oldPassword { get; set; }
        public string newPassword { get; set; }
        public string Username { get; set; }
    }
}