﻿using CanteenManagementSystem.API.Models;
using CanteenManagementSystem.API.Helpers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.WebSockets.Internal;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Threading.Tasks;

namespace CanteenManagementSystem.API.Services
{
    public interface IJwtService
    {
        Task<string> GenerateEncodedToken(string userName, ClaimsIdentity identity);

        ClaimsIdentity GenerateClaimsIdentity(string userName, string id, string claimValue);

        Guid GetCurrentUserId();
    }
    public class JwtService : IJwtService
    {
        private readonly JwtIssuerOptions _jwtOptions;
        private readonly ClaimsPrincipal _claimsPrincipal;

        public JwtService(IOptions<JwtIssuerOptions> jwtOptions, IHttpContextAccessor httpContextAccessor)
        {
            _jwtOptions = jwtOptions.Value;
            //ThrowIfInvalidOptions(_jwtOptions);
            _claimsPrincipal = httpContextAccessor.HttpContext == null ? null : httpContextAccessor.HttpContext.User;
        }

        public async Task<string> GenerateEncodedToken(string userName, ClaimsIdentity identity)
        {
            try
            {
                var claims = new[]
                 {
                 new Claim(JwtRegisteredClaimNames.Sub, userName),
                 new Claim(JwtRegisteredClaimNames.Jti, await _jwtOptions.JtiGenerator()),
                 //new Claim(JwtRegisteredClaimNames.Iat, ToUnixEpochDate(_jwtOptions.IssuedAt).ToString(), ClaimValueTypes.Integer64),
                 identity.FindFirst(ClaimTypes.Role),
                 identity.FindFirst(Helpers.Constants.Strings.JwtClaimIdentifiers.Id)
             };

                // Create the JWT security token and encode it.
                var jwt = new JwtSecurityToken(
                    issuer: _jwtOptions.Issuer,
                    audience: _jwtOptions.Audience,
                    claims: claims,
                    notBefore: _jwtOptions.NotBefore,
                    expires: _jwtOptions.Expiration,
                    signingCredentials: _jwtOptions.SigningCredentials);

                var encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);

                return encodedJwt;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public ClaimsIdentity GenerateClaimsIdentity(string userName, string id, string claimValue)
        {
            try
            {
                return new ClaimsIdentity(new GenericIdentity(userName, "Token"), new[]
                 {
                     new Claim(Helpers.Constants.Strings.JwtClaimIdentifiers.Id, id),
                     new Claim(ClaimTypes.Role, claimValue)
                 });
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Guid GetCurrentUserId()
        {
            Guid userId = Guid.Parse(_claimsPrincipal.Claims.Where(x => x.Type == ClaimTypes.NameIdentifier).FirstOrDefault().Value);
            return userId;
        }
    }
}
