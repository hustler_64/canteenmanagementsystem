﻿using CanteenManagementSystem.API.DbContext;
using CanteenManagementSystem.API.Models;
using CanteenManagementSystem.API.UnitOfWorks;
using CanteenManagementSystem.API.ViewModels;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace CanteenManagementSystem.API.Services
{
    #region Interface
    public interface IUserService<T> where T : class
    {
        Task<IdentityResult> CreateAsync(T userIdentity, string password);

        Task<IdentityResult> AddToRoleAsync(T userIdentity, string roleName);

        Task<T> FindByIdAsync(string userId);

        Task<T> FindByNameAsync(string userName);

        Task<bool> CheckPasswordAsync(T user, string password);

        Task<ClaimsIdentity> GetClaimsIdentity(string userName, string password);

        Task<IdentityResult> AddClaimAsync(T userIdentity, string roleName);

        Task<IList<Claim>> GetClaimsAsync(T userIdentity);

        ValidationMessage DeleteUser(Guid userId);
    }

    #endregion


    #region Service
    public class UserService<T> : IUserService<T> where T : AppUser
    {
        private readonly UserManager<T> _userManager;
        private readonly IJwtService _jwtService;
        private readonly CanteenManagementDbContext _dbContext;
        private readonly IUnitOfWork _unitOfWork;

        public UserService(UserManager<T> userManager, CanteenManagementDbContext dbContext, IJwtService jwtService, IUnitOfWork unitOfWork)
        {
            this._userManager = userManager;
            _dbContext = dbContext;
            this._jwtService = jwtService;
            _unitOfWork = unitOfWork;
        }

        public async Task<IdentityResult> CreateAsync(T userIdentity, string password)
        {
            try
            {
                var result = await _userManager.CreateAsync(userIdentity, password);
                if (result.Succeeded) await _dbContext.SaveChangesAsync();
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<IdentityResult> AddToRoleAsync(T userIdentity, string roleName)
        {
            try
            {
                var result = await _userManager.AddToRoleAsync(userIdentity, roleName);
                if (result.Succeeded) await _dbContext.SaveChangesAsync();
                return result;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public async Task<T> FindByNameAsync(string userName)
        {
            return await _userManager.FindByNameAsync(userName);
        }

        public async Task<T> FindByIdAsync(string userId)
        {
            return await _userManager.FindByIdAsync(userId);
        }

        public async Task<bool> CheckPasswordAsync(T user, string password)
        {
            return await _userManager.CheckPasswordAsync(user, password);
        }

        public async Task<ClaimsIdentity> GetClaimsIdentity(string userName, string password)
        {
            try
            {
                if (string.IsNullOrEmpty(userName) || string.IsNullOrEmpty(password))
                    return await Task.FromResult<ClaimsIdentity>(null);

                // get the user to verifty
                var userToVerify = await FindByNameAsync(userName);

                if (userToVerify == null) return await Task.FromResult<ClaimsIdentity>(null);

                var userClaims = await GetClaimsAsync(userToVerify);
                // check the credentials
                if (await CheckPasswordAsync(userToVerify, password))
                {
                    return await Task.FromResult(_jwtService.GenerateClaimsIdentity(userName, userToVerify.Id.ToString(), userClaims.SingleOrDefault().Value));
                }

                // Credentials are invalid, or account doesn't exist
                return await Task.FromResult<ClaimsIdentity>(null);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public async Task<IdentityResult> AddClaimAsync(T userIdentity, string roleName)
        {
            return await _userManager.AddClaimAsync(userIdentity, new Claim(ClaimTypes.Role, roleName));
        }

        public async Task<IList<Claim>> GetClaimsAsync(T userIdentity)
        {
            return await _userManager.GetClaimsAsync(userIdentity);
        }

        
        /// <summary>
        /// Blocks the selected user from the system
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public ValidationMessage DeleteUser(Guid userId)
        {
            var result = new ValidationMessage();
            var selectedUser = _unitOfWork.UserRepository.GetEntityById(userId);
            if (selectedUser != null)
            {
                if (!selectedUser.BlockUser)
                {
                    selectedUser.BlockUser = true;
                    _unitOfWork.UserRepository.UpdateEntity(selectedUser);
                    _unitOfWork.Commit();

                    result.Result = true;
                }
                else
                {
                    result.Result = false;
                    result.Message = "The user is already InActive";
                }
            }
            else
            {
                result.Result = false;
                result.Message = "User not found!";
            }

            return result;
        }
    }
    #endregion
}
