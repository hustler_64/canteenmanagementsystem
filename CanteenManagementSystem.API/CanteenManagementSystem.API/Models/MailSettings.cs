﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CanteenManagementSystem.API.Models
{
    public class MailSettings
    {
        [MaxLength(200)]
        public string Host { get; set; }

        [MaxLength(200)]
        public int Port { get; set; }

        [MaxLength(200)]
        public string AuthEmail { get; set; }

        [MaxLength(200)]
        public string AuthPassword { get; set; }
    }
}
