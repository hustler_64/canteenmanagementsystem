﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CanteenManagementSystem.API.Models
{
    public class AppRole : IdentityRole<Guid>
    {
    }
}
