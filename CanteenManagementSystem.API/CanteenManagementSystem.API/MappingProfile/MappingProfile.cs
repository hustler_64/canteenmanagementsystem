﻿using AutoMapper;
using CanteenManagementSystem.API.DataTransferObject;
using CanteenManagementSystem.API.Models;

namespace CanteenManagementSystem.API.MappingProfile
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<LoginDTO, AppUser>().ForMember(m => m.UserName, map => map.MapFrom(vm => vm.Username))
                .ForMember(m => m.Password, map => map.MapFrom(vm => vm.Password));
        }

        public override string ProfileName => base.ProfileName;
    }
}