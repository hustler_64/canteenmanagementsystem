﻿using CanteenManagementSystem.API.DbContext;
using CanteenManagementSystem.API.Interfaces;
using CanteenManagementSystem.API.Models;
using CanteenManagementSystem.API.ViewModels;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;

namespace CanteenManagementSystem.API.Data
{
    public class Account : IAccount
    {
        private readonly SignInManager<AppUser> _signInManager;
        private readonly UserManager<AppUser> _userManager;
        private readonly IConfiguration _configuration;
        private readonly CanteenManagementDbContext _dbcontext;
        private readonly IMailSender _mailSender;
        private readonly IOptions<MailSettings> _mailSettings;
        private const string Secret = "db3OIsj+BXE9NZDy0t8W3TcNekrF+2d/1sFnWG4HnV8TZY30iTOdtVWJG8abWvB1GlOgJuQZdcF2Luqm/hccMw==";

        public Account(SignInManager<AppUser> signInManager,
            UserManager<AppUser> userManager,
            IConfiguration configuration,
            CanteenManagementDbContext dbcontext,
            IMailSender mailSender,
            IOptions<MailSettings> mailsettings)
        {
            _signInManager = signInManager;
            _userManager = userManager;
            _configuration = configuration;
            _dbcontext = dbcontext;
            _mailSender = mailSender;
            _mailSettings = mailsettings;
        }

        public async Task<IdentityResult> AddUser(AppUser newUser, string confirmationLink)
        {
            var result = await _userManager.CreateAsync(newUser, newUser.Password);
            if (result.Succeeded)
            {
                var rolesAdded = await _userManager.AddToRoleAsync(newUser, "User");
            }
            //_mailSender.SendMailOnNewUser(_mailSettings, newUser.UserName, newUser.Email, confirmationLink);
            if (result.Succeeded == false)
            {
                var error = (result.Errors.FirstOrDefault() as IdentityError).Description;
                throw new Exception(error);
            }
            return result;
        }

        /// <summary>
        /// Sends email on new user creation
        /// </summary>
        public void SendConfirmationEmail(AppUser newUser, string confirmationLink)
        {
            _mailSender.SendMailOnNewUser(_mailSettings, newUser.UserName, newUser.Email, confirmationLink);
        }

        /// <summary>
        /// Sends confirmation link to users new email on update email
        /// </summary>
        public void SendConfirmationEmailOnNewEmail(AppUser newUser, string confirmationLink)
        {
            //_mailSender.SendMailOnChangedEmail(_mailSettings, newUser.UserName, newUser.Email, confirmationLink);
        }

        public async Task<object> DoLogin(string Username, string Password, bool isWebLogin)
        {
            var appUser = await _userManager.FindByNameAsync(Username);
            var result = await _userManager.CheckPasswordAsync(appUser, Password);
            if (result)
            {
                var role = await _userManager.GetRolesAsync(appUser);
                var token = GenerateJwtToken(Username, appUser, (List<string>)role);
                var firstLogin = appUser.FirstLogin;
                var userName = appUser.UserName;
                var userId = appUser.Id;
                if (isWebLogin)
                {
                    if (!role.Contains("Admin") && !role.Contains("SuperAdmin"))
                    {
                        var error = "Invalid login attempt.You do not have access to the site.";
                        throw new Exception(error);
                    }
                }

                if (!appUser.EmailConfirmed)
                {
                    var error = "Invalid login attempt.You must have a confirmed email account.";
                    throw new Exception(error);
                }

                var loginData = new { role, token, firstLogin, userName, userId };
                return loginData;
            }
            else
            {
                var error = "Invalid login attempt.Invalid Username or Password!.";
                throw new Exception(error);
            }
        }

        private object GenerateJwtToken(string username, AppUser user, List<string> role)
        {
            var claims = new List<Claim>
            {
                new Claim(JwtRegisteredClaimNames.Sub, username),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()),
                new Claim(ClaimTypes.Role, string.Join(",",role))
            };

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["JwtKey"]));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            var expires = DateTime.Now.AddHours(Convert.ToDouble(_configuration["JwtExpireHour"]));

            var token = new JwtSecurityToken(
                _configuration["JwtIssuer"],
                _configuration["JwtIssuer"],
                claims,
                expires: expires,
                signingCredentials: creds
            );

            return new JwtSecurityTokenHandler().WriteToken(token);
        }

        public async Task<ValidationMessage> ForgotPassword(string userEmail)
        {
            var result = new ValidationMessage();
            var userFromDatabase = await _userManager.FindByEmailAsync(userEmail);
            if (userFromDatabase == null)
            {
                result.Result = false;
                result.Message = "Email not found, Please register.";
                return result;
            }

            var code = await _userManager.GeneratePasswordResetTokenAsync(userFromDatabase);
            var encodeCode = HttpUtility.UrlEncode(code, Encoding.ASCII);
            //_mailSender.SendMailOnResetPassword(_mailSettings, userFromDatabase.UserName, encodeCode, userFromDatabase.Email, userFromDatabase.Id);

            //string body = "<h2>Reset Password</h2>" +
            //    $"Code: {encodeCode} <br>" +
            //    $"UserId: {userFromDatabase.Id}<br><br>" +
            //    $"Reset Link: {apiUrl}/ResetPassword/ResetPassword?token=" + encodeCode + "&userId=" + userFromDatabase.Id + "&username=" + userFromDatabase.UserName;
            ////SetupMailSettings();
            //List<string> s = new List<string> { userEmail };
            //_mailSender.SendMail(s, "Password Reset", body, _mailSettings, null);
            result.Result = true;
            result.Message = "Confirmation Email Sent";
            return result;
        }

        public async Task<ValidationMessage> ConfirmReset(string token, string userId, string newPassword)
        {
            var data = new ValidationMessage();
            if (token != null && userId != null)
            {
                var userFromDatabase = await _userManager.FindByIdAsync(userId);
                var result = await _userManager.ResetPasswordAsync(userFromDatabase, token, newPassword);
                if (result.Succeeded)
                {
                    data.Result = true;
                    data.Message = "Change Success";
                    return data;
                }
                else
                {
                    data.Result = false;
                    data.Message = (result.Errors).FirstOrDefault().Description;
                    return data;
                }
            }
            data.Result = false;
            data.Message = "Invalid Token";
            return data;
        }

        public bool IsValidEmailAddress(string email)
        {
            var regex = new Regex(@"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?");
            return regex.IsMatch(email);
        }
    }
}