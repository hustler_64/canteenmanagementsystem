﻿using CanteenManagementSystem.API.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CanteenManagementSystem.API.DbContext
{   
    public class CanteenManagementDbContext : IdentityDbContext<AppUser, AppRole, Guid>
    {
        public CanteenManagementDbContext(DbContextOptions<CanteenManagementDbContext> options)
           : base(options)
        {
        }
    }
}
